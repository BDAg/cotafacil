from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
import sys
import json
#bot de cotação mapfre
url = "https://www.mapfreconnect.com.br/"

driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
driver.get(url)
delay = 3

def login():
    escolheop = driver.find_element_by_xpath("//*[@id='CodVc']")
    escolheop.clear()
    escolheop.send_keys('100480312')
    escolheop = driver.find_element_by_xpath("//*[@id='CodVc']")
    escolheop.clear()
    escolheop.send_keys('100480312')
    ci = driver.find_element_by_xpath("//*[@id='CodInt']")
    ci.clear()
    ci.send_keys("72641")
    senha = driver.find_element_by_xpath("//*[@id='SenhaVc']")
    senha.send_keys("Pa124578%")
    ci.send_keys(Keys.TAB)
    time.sleep(3)
    ci.send_keys(Keys.TAB)
    time.sleep(3)
    co = driver.find_element_by_xpath('//*[@id="sel_companhia"]')
    co.send_keys(Keys.TAB)
    time.sleep(3)
    usuario = driver.find_element_by_xpath("//*[@id='cd_usuario']/option[2]")
    usuario.click()
    time.sleep(3)
    entrar = driver.find_element_by_xpath("//*[@id='btnCalculo']")
    entrar.click()
    time.sleep(3)

def novoCalculo():
    calcular = driver.find_element_by_id('MenuDinamico1_bt_neg')
    calcular.click()
    time.sleep(1)
    automovel = driver.find_element_by_id('MenuDinamico1_MenuProduto_menuItem000')
    automovel.click()
    time.sleep(1)
    novo = driver.find_element_by_id('MenuDinamico1_MenuProduto_menuItem000_subMenu_menuItem001')
    novo.click()
    time.sleep(1)

# preenche o questionario do calculo
def segurado(cpfCliente):
    iframe = driver.find_element_by_xpath('//*[@id="ifrConteudo"]')
    driver.switch_to.frame(iframe)
    cpf = driver.find_element_by_xpath('//*[@id="formSeguroSegurado:cpfCnpj"]')
    cpf.send_keys(cpfCliente)
    cpf.send_keys(Keys.TAB)
    time.sleep(3)
    envioEmail = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/section/div/div[1]/span/div/div[8]/span/div/span/input')
    envioEmail.click()
    time.sleep(1)
    prox = driver.find_element_by_xpath('//*[@id="formSeguroSegurado:btoProximo"]')
    prox.click()
    driver.switch_to.default_content()
    time.sleep(2)

def veiculo(chassiDoc):
    iframe = driver.find_element_by_xpath('//*[@id="ifrConteudo"]')
    driver.switch_to.frame(iframe)
    chassi = driver.find_element_by_xpath('//*[@id="formVeiculo:chassi"]')
    chassi.send_keys(chassiDoc)
    chassi.send_keys(Keys.TAB)
    time.sleep(2)
    modalCarro = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/div[1]')
    modalCarro.get_attribute('class')
    print(modalCarro.get_attribute('class'))
    if (modalCarro.get_attribute('class')=="window ui-draggable modal-window"):
        selecionaVeiculo = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/div[1]/form/span/div/span/div[1]/div/ul/li[1]')
        selecionaVeiculo.click()
        time.sleep(1)
        salvar = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/div[1]/form/span/div/span/div[3]/a[2]')
        salvar.click()
        time.sleep(1)
    uso = Select(driver.find_element_by_xpath('//*[@id="formVeiculo:uso"]'))
    uso.select_by_value('1')
    time.sleep(1)
    prox2 = driver.find_element_by_xpath('//*[@id="formVeiculo:btoProximo"]')
    prox2.click() 
    driver.switch_to.default_content()
    time.sleep(3)

def condutor(segurad, cpfCondutor, nomeCondutor, dataNascimentoCondutor,sexoCondutor):
    iframe = driver.find_element_by_xpath('//*[@id="ifrConteudo"]')
    driver.switch_to.frame(iframe)
    if (segurad == "1"):
        questionario = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[1]/table/tbody/tr/td[2]/div/span/input')
        questionario.click()
    else:
        questionario = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[1]/table/tbody/tr/td[1]/div/span/input')
        questionario.click()
        time.sleep(2)
        cpf = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[3]/input')
        cpf.click()
        cpf.send_keys(cpfCondutor)
        nome = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[4]/input')
        nome.click()
        nome.send_keys(nomeCondutor)
        dataNascimento = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[5]/input')
        dataNascimento.click
        dataNascimento.send_keys(dataNascimentoCondutor)
        if sexoCondutor == "1" :
            sexo = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[6]/table/tbody/tr/td[2]/div/span/input')
            sexo.click()
        else:
            sexo = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[6]/table/tbody/tr/td[1]/div/span/input')
            sexo.click()
    estCivil = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[8]/div/select'))
    estCivil.select_by_value('5')
    profissao = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[9]/div/select'))
    profissao.select_by_value('342')
    tempoHabilitacao = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[10]/div/select'))
    tempoHabilitacao.select_by_value('13')
    tipoResidencia = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[11]/div/select'))
    tipoResidencia.select_by_value('579')
    time.sleep(2)
    menorCondutor = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[13]/table/tbody/tr/td[1]/div/span/input')
    menorCondutor.click()
    resideMenor = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[14]/table/tbody/tr/td[1]/div/span/input')
    resideMenor.click()
    veiculosResidencia = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[1]/div[2]/span/span[16]/div/select'))
    veiculosResidencia.select_by_value('28')

def usoVeiculo(cepPernoite):
    time.sleep(1)
    cep = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[1]/span[1]/input')
    cep.click()
    cep.send_keys(cepPernoite)
    time.sleep(2)
    proprietario = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[3]/div/select'))
    proprietario.select_by_value('630')
    visitaCliente = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[4]/table/tbody/tr/td[1]/div/span/input')
    visitaCliente.click()
    alienado = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[6]/table/tbody/tr/td[1]/div/span/input')
    alienado.click()
    antiFurto = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[7]/span/table/tbody/tr/td[1]/div/span/input')
    antiFurto.click()
    existeGaragem = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[8]/span[2]/table/tbody/tr/td[2]/div/span/input')
    existeGaragem.click()
    time.sleep(3)
    residencia = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[8]/span[3]/div/select'))
    residencia.select_by_value('1')
    trabalho = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[8]/span[4]/div/select'))
    trabalho.select_by_value('146')
    faculdade = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div[1]/span/span[2]/div[3]/div[2]/span/span[8]/span[5]/div/select'))
    faculdade.select_by_value('1483')
    time.sleep(2)
    prox = driver.find_element_by_xpath('//*[@id="formAvaliacaoRisco:btoProximo"]')
    prox.click()
    time.sleep(2)

def coberturas():
    franquia = Select(driver.find_element_by_xpath('//*[@id="formCoberturas:idFranquia_0"]'))
    franquia.select_by_value('Reduzida')
    danosMorais = Select(driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/div/div[1]/div[2]/div/div[3]/table/tbody/tr[2]/td[2]/div/div/select'))
    danosMorais.select_by_value('21')
    danosMateriais = Select(driver.find_element_by_xpath('//*[@id="formCoberturas:coberturasList_0:2:idOpcao_0"]'))
    danosMateriais.select_by_value('39')
    danosCorporais = Select(driver.find_element_by_xpath('//*[@id="formCoberturas:coberturasList_0:3:idOpcao_0"]'))
    danosCorporais.select_by_value('39')
    vidros = Select(driver.find_element_by_xpath('//*[@id="formCoberturas:coberturasAdicionais_0:0:idOpcaoFavoritaAdicional_0"]'))
    vidros.select_by_value('170756')
    reboque = Select(driver.find_element_by_xpath('//*[@id="formCoberturas:coberturasAdicionais_0:1:idOpcaoFavoritaAdicional_0"]'))
    reboque.select_by_value('1701163')
    carroReserva = Select(driver.find_element_by_xpath('//*[@id="formCoberturas:coberturasAdicionais_0:2:idOpcaoFavoritaAdicional_0"]'))
    carroReserva.select_by_value('170757')
    apoMorte = driver.find_element_by_xpath('//*[@id="formCoberturas:coberturasAdicionais_0:4:idOpcaoTextAdicional_0"]')
    apoMorte.clear()
    apoMorte.send_keys('500000')
    apoInvalidez = driver.find_element_by_xpath('//*[@id="formCoberturas:coberturasAdicionais_0:5:idOpcaoTextAdicional_0"]')
    apoInvalidez.clear()
    apoInvalidez.send_keys('500000')
    prox = driver.find_element_by_xpath('//*[@id="formCoberturas:save"]')
    prox.click()
    time.sleep(3)

def valor():
    banco = driver.find_element_by_xpath('//*[@id="frmCalculoCotacao:nomeBancoInput"]')
    banco.send_keys('000')
    time.sleep(3)
    Keys.ENTER
    time.sleep(1)
    operacao = driver.find_element_by_xpath('//*[@id="frmCalculoCotacao:codigoComissao"]')
    operacao.clear()
    operacao.send_keys('10', Keys.TAB)
    time.sleep(2)
    calcular = driver.find_element_by_xpath('//*[@id="frmCalculoCotacao:btnCalcular"]')
    calcular.click()
    time.sleep(7)
    valorTotalFranquiaReduzida = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/section/div/span[3]/span[1]/div/div[1]/div[3]/div[1]/ul/li[4]/span/label').text
    valorTotal = driver.find_element_by_xpath('/html/body/div[1]/span/div/div/form/section/div/span[3]/span[1]/div/div[2]/div[3]/div[1]/ul/li[4]/span/label').text
    numeroCotacao = driver.find_element_by_xpath('/html/body/div[1]/div[2]/form/span/span/div[1]/ul/li[1]/div/span[2]/label[2]').text
    
    print(valorTotalFranquiaReduzida+'/'+valorTotal+'/'+numeroCotacao)
    

cpf = str(sys.argv[1])
chassi = str(sys.argv[4])
cpfCondutor = str(sys.argv[4])
nomeCondutor = str(sys.argv[4]) 
dataNascimentoCondutor = str(sys.argv[5])
cepPernoite = str(sys.argv[8])
segurad = str(sys.argv[9])
sexoCondutor = str(sys.argv[10])

login()
novoCalculo()
segurado(cpf)
veiculo(chassi)
condutor(segurad,cpfCondutor, nomeCondutor, dataNascimentoCondutor,sexoCondutor)
usoVeiculo(cepPernoite)
coberturas()
valor()

