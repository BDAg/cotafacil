from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
import sys
import json
import re


url = "https://www.hdi.com.br/hdidigital/"

driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
driver.get(url)
delay = 3

def login():
    usuario = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div/div[2]/div[1]/input")
    usuario.send_keys("06620709842") 
    senha = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div/div[2]/div[2]/input")
    senha.send_keys("Ap784512")
    entrar = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div/div[2]/button")
    entrar.click()
    time.sleep(2)

def novo_calculo():
    novo = driver.find_element_by_xpath("/html/body/nav[2]/ul/li[2]/a")
    novo.click()
    novo2 = driver.find_element_by_xpath("/html/body/nav[2]/ul/li[2]/ul/li[1]/a")
    novo2.click()

def segurado(cpf1, nomeSegurado):
    time.sleep(2)
    cpf = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[1]/div/div[3]/fieldset/div/div[4]/div/input")
    time.sleep(1)
    cpf.send_keys(cpf1)
    nome = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[1]/div/div[3]/fieldset/div/div[5]/div/input")
    nome.click()
    time.sleep(1)
    nome.clear()
    nome.send_keys(nomeSegurado)

def veiculo(chassi1,cep1):
    chassi = driver.find_element_by_xpath("//*[@id='chassi']")
    chassi.send_keys(chassi1)
    Keys.TAB
    time.sleep(2)
    okChassi = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[7]/div/div[3]/div/fieldset/div[3]/div[1]/button")
    okChassi.click()
    okChassi.click()
    time.sleep(5)
    confirmaCarro = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/span/table/tbody/tr")
    confirmaCarro.click()
    time.sleep(1)
    tipo_seguro = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[7]/div/div[3]/div/fieldset/div[13]/div[2]/label[1]/input")
    tipo_seguro.click()
    cep = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[7]/div/div[3]/div/fieldset/div[15]/div/div[2]/div/input")
    cep.send_keys(cep1)

def principal_condutor(nome,nascimento, sexoCondutor):
    nomeCondutor = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/fieldset/div/div/div[2]/div/input")
    nomeCondutor.clear()
    nomeCondutor.send_keys(nome)
    dataNascimento = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/fieldset/div/div/div[3]/div/input")
    dataNascimento.click()
    dataNascimento.clear()
    dataNascimento.send_keys(nascimento)
    time.sleep(2)
    modal = driver.find_element_by_xpath('/html/body/div[3]/div/div/div')
    modal = modal.get_attribute('class')
    time.sleep(1)
    if (modal == 'modal hide mensagem in'):
        fechar = driver.find_element_by_xpath("/html/body/div[3]/div/div/div/div/div/div[1]/span")
        fechar.click()
        time.sleep(2)
    if(sexoCondutor == "1"):
        sexo = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/fieldset/div/div/div[4]/div/label[1]/input")
        sexo.click()
    else :
        sexo = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/fieldset/div/div/div[4]/div/label[2]/input")
        sexo.click()
        
    estado_civil = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/fieldset/div/div/div[5]/div/select")
    estado_civil.click()
    tipo_relacionamento = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/fieldset/div/div/div[5]/div/select/option[2]")
    tipo_relacionamento.click()

def questionario_risco():
    menor_condutor = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/div[2]/div/div[2]/label[2]/input")
    menor_condutor.click()
    garagem_residencia = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/div[3]/div[2]/div/label[1]/input")
    garagem_residencia.click()
    garagem_trabalho = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/div[3]/div[3]/div/label[2]/input")
    garagem_trabalho.click()
    utilizacao = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/div[4]/div/div[2]/select")
    utilizacao.click()
    uso_diario = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[9]/div/div/div/div[3]/div/fieldset/div/div[4]/div/div[2]/select/option[2]")
    uso_diario.click()

def garantia():
    franquia = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[1]/div/select")
    franquia.click()
    seleciona_franquia = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[1]/div/select/option[5]")
    seleciona_franquia.click()
    valor_de_mercado = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[3]/div/input")
    valor_de_mercado.clear()
    valor_de_mercado.send_keys("100")
    danos_materiais = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[6]/div/select")
    danos_materiais.click()
    valor_danos_materiais = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[6]/div/select/option[7]")
    valor_danos_materiais.click()
    danos_corporais = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[7]/div/select")
    danos_corporais.click()
    valor_corporais = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[7]/div/select/option[7]")
    valor_corporais.click() 
    morte_invalidez = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[10]/div/div[3]/div/fieldset/div[2]/div[8]/div/input")
    morte_invalidez.clear()
    morte_invalidez.send_keys("500000")
    coberturas = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[11]/div/div[3]/div[2]/fieldset/div/div[1]/div/div[3]/label/input")
    coberturas.click()
    time.sleep(3)
    carro_reserva = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[11]/div/div[3]/div[2]/fieldset/div/div[2]/div[2]/div[2]/div[2]/input")
    carro_reserva.click()
    vidro = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[11]/div/div[3]/div[2]/fieldset/div/div[2]/div[3]/div[2]/div[1]/input")
    vidro.click()
    retrovisor_lanterna_farol = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[1]/div[3]/form/div[11]/div/div[3]/div[2]/fieldset/div/div[2]/div[3]/div[2]/div[2]/input")
    retrovisor_lanterna_farol.click()
    calcular = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[2]/div/div[5]/div[1]/div[1]/button")
    calcular.click()

def valor():
    time.sleep(10)
    fechaModal = driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[4]/div/div[1]/button')
    fechaModal.click()
    time.sleep(10)
    valorTotal = driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div/div[2]/div/div[4]/div/div/form/div[1]/div[5]/div/input')
    numOrcamento = driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div/div[1]/p[1]/span').text
    print(valorTotal.get_attribute('value')+'/'+numOrcamento)

cpf = str(sys.argv[1])
nomeSegurado = str(sys.argv[2])
nascimentoSegurado =str(sys.argv[3]).replace('/','')
chassi = str(sys.argv[4]) 
dataNascimentoCondutor = str(sys.argv[5])
cepPernoite = str(sys.argv[6])
segurad = str(sys.argv[7])
cep = str(sys.argv[8])
segurado1 = str(sys.argv[9])
sexo = str(sys.argv[10])
print(cpf, nomeSegurado, nascimentoSegurado, chassi, dataNascimentoCondutor, cepPernoite, segurado1, cep, sexo)
time.sleep(3)
login()
time.sleep(5)
novo_calculo()
time.sleep(3)
segurado(cpf, nomeSegurado)
veiculo(chassi, cep)
principal_condutor(nomeSegurado, nascimentoSegurado, sexo)
questionario_risco()
garantia()
valor()