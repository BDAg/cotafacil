const mysql = require("../mysql");
const { PythonShell } = require("python-shell");

exports.bot = () => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }

    conn.query(
      `INSERT INTO orcamentos(seguradora, franquia_normal, franquia_reduzida, idusuarios, idsegurados, idveiculos) VALUES ('mapfre', ?, ?, 1, ?, ?);`,
      [results[10], results[11], segurado.idsegurado, segurado.idveiculo],
      (error, results) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          result: results,
        });
      }
    );
  });
};

exports.ativaBot1 = async (req, res, next) => {
  let segurado = {};

  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `
                    select segurados.idsegurados,
                    	   segurados.nome,
                           segurados.cpf,
                           segurados.data_nascimento,
                           veiculos.idveiculos,
                           veiculos.chassi,
                           veiculos.placa,
                           veiculos.ano_fabricacao,
                           veiculos.ano_modelo,
                           veiculos.veiculos,
                           veiculos.cep_pernoite,
                           principais_condutores.cpf AS cpfCondutor,
                           principais_condutores.nome AS nomeCondutor,
                           principais_condutores.data_nascimento AS nascimentoCondutor,
                           principais_condutores.sexo AS sexoCondutor,
                           principais_condutores.estado_civil AS estCivilCondutor,
                           principais_condutores.segurado
                      from segurados 
                     inner join veiculos 
                     inner join principais_condutores 
                     inner join questionarios_de_risco 
                     inner join coberturas
                     where segurados.idsegurados = veiculos.idsegurados and 
                    	   segurados.idsegurados = ? and 
                           veiculos.idveiculos = principais_condutores.idveiculos and 
                    	   veiculos.idveiculos = questionarios_de_risco.idveiculos and
                           veiculos.idveiculos = coberturas.idveiculos;
        `,
      [req.params.idsegurado],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        if (results.length > 0) {
          segurado["idsegurado"] = results[0].idsegurados;
          segurado["cpf"] = results[0].cpf;
          segurado["nome"] = results[0].nome;
          segurado["data_nascimento"] = results[0].data_nascimento;
          segurado["chassi"] = results[0].chassi;
          segurado["cep_pernoite"] = results[0].cep_pernoite;
          segurado["cpfCondutor"] = results[0].cpfCondutor;
          segurado["nomeCondutor"] = results[0].nomeCondutor;
          segurado["nascimentoCondutor"] = results[0].nascimentoCondutor;
          segurado["segurado"] = results[0].segurado;
          segurado["sexo"] = results[0].sexoCondutor;
          segurado["idveiculo"] = results[0].idveiculos;

          let options = {
            mode: "text",
            pythonPath: "python3",
            pythonOptions: ["-u"], // get print results in real-time
            scriptPath: "../bots/",
            args: [
              segurado.cpf,
              segurado.nome,
              segurado.data_nascimento,
              segurado.chassi,
              segurado.cpfCondutor,
              segurado.nomeCondutor,
              segurado.nascimentoCondutor,
              segurado.cep_pernoite,
              segurado.segurado,
              parseInt(segurado.sexo),
            ],
          };

          PythonShell.run("bot1.py", options, function (err, results) {
            if (err) console.log(err);
            setTimeout(() => {
              console.log(results);
              if (results == null) {
                console.log(results);
              } else if (results.length == 2){
                console.log(results.length);
                this.valor = results[1];
                this.valor = this.valor.split("/");
                console.log(this.valor)
                console.log(segurado)
                mysql.getConnection((err, conn) => {
                  if (err) {
                    return res.json(error);
                  }

                  conn.query(
                    `INSERT INTO orcamentos(seguradora, franquia_normal, franquia_reduzida, num_orcamento,idusuarios, idsegurados, idveiculos) VALUES ('mapfre', ?, ?, ?, 1, ?, ?);`,
                    [
                      this.valor[1],
                      this.valor[0],
                      this.valor[2],
                      segurado.idsegurado,
                      segurado.idveiculo,
                    ],
                    (error, results) => {
                      conn.release();
                      if (error) {
                        return res.json(error);
                      }
                      console.log(results);
                      return res.json(this.valor);
                    }
                  );
                });
              }
            }, 500);
          });
         
        } else {
          return res.status(200).send({
            mensagem: "segurado nao encontrado",
          });
        }
      }
    );
  });
};
exports.ativaBot2 = async (req, res, next) => {
    let segurado = {};
  
    mysql.getConnection((err, conn) => {
      if (err) {
        return res.status(500).send({ error: err });
      }
      conn.query(
        `
                      select segurados.idsegurados,
                             segurados.nome,
                             segurados.cpf,
                             segurados.data_nascimento,
                             veiculos.idveiculos,
                             veiculos.chassi,
                             veiculos.placa,
                             veiculos.ano_fabricacao,
                             veiculos.ano_modelo,
                             veiculos.veiculos,
                             veiculos.cep_pernoite,
                             principais_condutores.cpf AS cpfCondutor,
                             principais_condutores.nome AS nomeCondutor,
                             principais_condutores.data_nascimento AS nascimentoCondutor,
                             principais_condutores.sexo AS sexoCondutor,
                             principais_condutores.estado_civil AS estCivilCondutor,
                             principais_condutores.segurado
                        from segurados 
                       inner join veiculos 
                       inner join principais_condutores 
                       inner join questionarios_de_risco 
                       inner join coberturas
                       where segurados.idsegurados = veiculos.idsegurados and 
                             segurados.idsegurados = ? and 
                             veiculos.idveiculos = principais_condutores.idveiculos and 
                             veiculos.idveiculos = questionarios_de_risco.idveiculos and
                             veiculos.idveiculos = coberturas.idveiculos;
          `,
        [req.params.idsegurado],
        (error, results) => {
          if (error) {
            return res.status(500).send({ error: error });
          }
          if (results.length > 0) {
            segurado["idsegurado"] = results[0].idsegurados;
            segurado["cpf"] = results[0].cpf;
            segurado["nome"] = results[0].nome;
            segurado["data_nascimento"] = results[0].data_nascimento;
            segurado["chassi"] = results[0].chassi;
            segurado["cep_pernoite"] = results[0].cep_pernoite;
            segurado["cpfCondutor"] = results[0].cpfCondutor;
            segurado["nomeCondutor"] = results[0].nomeCondutor;
            segurado["nascimentoCondutor"] = results[0].nascimentoCondutor;
            segurado["segurado"] = results[0].segurado;
            segurado["sexo"] = results[0].sexoCondutor;
            segurado["idveiculo"] = results[0].idveiculos;
  
            let options = {
              mode: "text",
              pythonPath: "python3",
              pythonOptions: ["-u"], // get print results in real-time
              scriptPath: "../bots/",
              args: [
                segurado.cpf,
                segurado.nome,
                segurado.data_nascimento,
                segurado.chassi,
                segurado.cpfCondutor,
                segurado.nomeCondutor,
                segurado.nascimentoCondutor,
                segurado.cep_pernoite,
                segurado.segurado,
                parseInt(segurado.sexo),
              ],
            };

            PythonShell.run('bot2.py', options, function(err, results) {
                if (err) console.log(err);
                setTimeout(() => {
                    console.log(results);
                    if (results == null) {
                      console.log(results);
                    } else if (results.length == 2){
                      console.log(results.length);
                      console.log(results)
                      this.valor = results[1];
                      this.valor = this.valor.split("/");
      
                      mysql.getConnection((err, conn) => {
                        if (err) {
                          return res.json(error);
                        }
      
                        conn.query(
                          `INSERT INTO orcamentos(seguradora, franquia_normal, franquia_reduzida, num_orcamento,idusuarios, idsegurados, idveiculos) VALUES ('hdi', 'nao-cotado', ?, ?, 1, ?, ?);`,
                          [
                            this.valor[0],
                            this.valor[1],
                            segurado.idsegurado,
                            segurado.idveiculo,
                          ],
                          (error, results) => {
                            conn.release();
                            if (error) {
                              return res.json(error);
                            }
                            console.log(results);
                            return res.json(this.valor);
                          }
                        );
                      });
                    }
                  }, 1000);
            });
          } else {
            return res.status(200).send({
              mensagem: "segurado nao encontrado",
            });
          }
        }
      );
    });
  };

exports.buscaMapfre = (req, res) => {
  let options = {
    mode: "text",
    pythonPath: "python3",
    pythonOptions: ["-u"], // get print results in real-time
    scriptPath: "../bots/",
    args: [
      req.body.codigo
    ],
  };
  PythonShell.run('bot4.py', options, function(err, results) {
    if (err) console.log(err);
    return results
  })
  return res.send('ok')
}