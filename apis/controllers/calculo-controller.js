const mysql = require("../mysql");

exports.cadastroSegurado = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `
                    INSERT INTO segurados(cpf, nome, data_nascimento) VALUES (?, ?, ?)
                    `,
      [req.body.cpf, req.body.nome, req.body.data_nascimento],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.updateSegurado = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `
                UPDATE segurados	
                SET cpf = ?,
                    nome = ?,
                    data_nascimento = ?
                WHERE idsegurados = ?;
                    `,
      [
        req.body.cpf,
        req.body.nome,
        req.body.data_nascimento,
        req.body.idsegurados,
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results,
        });
      }
    );
    conn.release();
  });
};

exports.cadastroVeiculo = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `
                INSERT INTO veiculos(chassi, placa, ano_fabricacao, ano_modelo, veiculos, cep_pernoite, idsegurados) VALUES (?, ?, ?, ?, ?, ?, ?);
                    `,
      [
        req.body.chassi,
        req.body.placa,
        req.body.ano_fabricacao,
        req.body.ano_modelo,
        req.body.veiculos,
        req.body.cep_pernoite,
        req.body.idsegurados,
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.updateVeiculo = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `
        UPDATE veiculos
        SET chassi = ?,
            placa = ?,
            ano_fabricacao = ?,
            ano_modelo = ?,
            veiculos = ?,
            cep_pernoite = ?
        WHERE idveiculos = ?;
        
                    `,
      [
        req.body.chassi,
        req.body.placa,
        req.body.ano_fabricacao,
        req.body.ano_modelo,
        req.body.veiculos,
        req.body.cep_pernoite,
        req.body.idveiculos,
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results,
        });
      }
    );
    conn.release();
  });
};

exports.cadastroPrincipalCondutor = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    console.log(req.body);
    conn.query(
      `INSERT INTO principais_condutores(cpf, nome, data_nascimento, sexo, estado_civil, segurado, idveiculos) values (? , ?, ?, ?, ?, ?, ?);`,
      [
        req.body.cpf,
        req.body.nome,
        req.body.data_nascimento,
        req.body.sexo,
        req.body.estado_civil,
        parseInt(req.body.segurado),
        parseInt(req.body.idveiculos),
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.updatePrincipalCondutor = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    console.log(req.body);
    conn.query(
      `UPDATE principais_condutores
                SET cpf = ?,
                    nome = ?,
                    data_nascimento = ?,
                    sexo = ?,
                    estado_civil = ?,
                    segurado = ?
              WHERE idveiculos = ?;`,
      [
        req.body.cpf,
        req.body.nome,
        req.body.data_nascimento,
        req.body.sexo,
        req.body.estado_civil,
        parseInt(req.body.segurado),
        parseInt(req.body.idveiculos),
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.cadastroQuestionarioRisco = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `INSERT INTO questionarios_de_risco(utilizacao, garagem_casa, garagem_servico, garagem_escola, menor_condutor, idveiculos) values (? , ?, ?, ?, ?, ?);`,
      [
        req.body.utilizacao,
        req.body.garagem_casa,
        req.body.garagem_servico,
        req.body.garagem_escola,
        req.body.menor_condutor,
        req.body.idveiculos,
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.updateQuestionarioRisco = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `UPDATE questionarios_de_risco
            SET utilizacao = ?,
                garagem_casa = ?,
                garagem_servico = ?,
                garagem_escola = ?,
                menor_condutor = ?
          WHERE idveiculos = ?;`,
      [
        req.body.utilizacao,
        req.body.garagem_casa,
        req.body.garagem_servico,
        req.body.garagem_escola,
        req.body.menor_condutor,
        req.body.idveiculos,
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.cadastroCoberturas = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `INSERT INTO coberturas(
                danos_morais_esteticos, 
                danos_materiais, 
                danos_corporais, 
                morte_invalidez_ocupante, 
                limite_guincho, 
                carro_reserva,
                vidros,
                farol_auxiliar,
                retrovisor_lanterna_farol,
                idveiculos
                ) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`,
      [
        req.body.danos_morais_esteticos,
        req.body.danos_materiais,
        req.body.danos_corporais,
        req.body.morte_invalidez_ocupante,
        req.body.limite_guincho,
        req.body.carro_reserva,
        req.body.vidros,
        req.body.farol_auxiliar,
        req.body.retrovisor_lanterna_farol,
        req.body.idveiculos,
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.updateCoberturas = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `UPDATE coberturas
            SET danos_morais_esteticos = ?,
                danos_materiais = ?,
                danos_corporais = ?,
                morte_invalidez_ocupante = ?,
                limite_guincho = ?,
                carro_reserva = ?, 
                vidros = ?,
                farol_auxiliar = ?,
                retrovisor_lanterna_farol = ?
          WHERE idveiculos = ?;`,
      [
        req.body.danos_morais_esteticos,
        req.body.danos_materiais,
        req.body.danos_corporais,
        req.body.morte_invalidez_ocupante,
        req.body.limite_guincho,
        req.body.carro_reserva,
        req.body.vidros,
        req.body.farol_auxiliar,
        req.body.retrovisor_lanterna_farol,
        req.body.idveiculos,
      ],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        return res.status(200).send({
          results: results.insertId,
        });
      }
    );
    conn.release();
  });
};

exports.getCalculo = (req, res, next) => {
  mysql.getConnection((err, conn) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    conn.query(
      `     select orcamentos.*, 
                   segurados.nome, 
                   veiculos.veiculos 
              from orcamentos 
        inner join segurados 
        inner join veiculos 
        where idusuarios = ? 
          and segurados.idsegurados = orcamentos.idsegurados 
          and orcamentos.idveiculos = veiculos.idveiculos
        order by orcamentos.idorcamentos desc;
                        `,
      [req.params.idusuario],
      (error, results) => {
        conn.release()
        if (error) {
        return res.status(500).send({ error: error });
      }
      return res.status(200).send({
        results
      })
    }

    );
  });
};
