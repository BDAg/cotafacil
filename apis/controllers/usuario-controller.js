const { response } = require('../app');
const mysql = require('../mysql');
const LocalStorage = require('node-localstorage').LocalStorage
localStorage = new LocalStorage('./scratch');

exports.criaUsuario = (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err){
            return res.status(500).send({ error: err });
        }

        conn.query(`
            SELECT email FROM usuarios WHERE email = ?
        `, [req.body.email], (error, results) => {
            if (error) {
                return res.status(500).send({ error: error });
            } else if (results.length > 0) {
                return res.status(200).send({ mensagem: "usuario ja cadastrado" })
            } else {
                conn.query(`
                    INSERT INTO usuarios(nome, email, cel, senha) VALUES (?, ?, ?, ?)
                    `,[
                        req.body.nome,
                        req.body.email,
                        req.body.cel,
                        req.body.senha
                    ],
                    (error2, results2) => {
                        if (error){
                            return res.status(500).send({ error: error2 });
                        }
                        return res.status(200).send({
                            results: results2
                        })
                });
            }
        })
        conn.release();
    });
}

exports.listaUsuarios = (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err){
            return res.status(500).send({ error: err });
        }
        conn.query(`
            SELECT * FROM usuarios
        `, (error, results) => {
            if (error) {
                return res.status(500).send({ error: error });
            }
            return res.status(200).send({
                result: results
            })
        });
    });
}

exports.login = (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if(err) {
            return res.status(500).send({ error: err})
        }
        conn.query(`
            SELECT * FROM usuarios WHERE email = ? AND senha = ?;
        `,[req.body.email, req.body.senha],(error, results) => {
            conn.release()
            if (error) {
                return res.status(500).send({ error: error });
            }
            if (results.length > 0) {
                return res.status(200).send({
                    result: results
                })
            } else {
                return res.status(200).send({
                    mensagem: 'usuario nao encontrado'
                })
            }
        })
    })
}