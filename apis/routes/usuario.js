const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuario-controller');

router.post('/cadastro', usuarioController.criaUsuario);
router.get('/lista',usuarioController.listaUsuarios);
router.post('/login', usuarioController.login);

module.exports = router;