const express = require('express');
const router = express.Router();
const calculoController = require('../controllers/calculo-controller');
const botController = require('../controllers/bots-controller');

router.post('/segurado', calculoController.cadastroSegurado);
router.put('/update-segurado', calculoController.updateSegurado);
router.post('/veiculo', calculoController.cadastroVeiculo);
router.put('/update-veiculos', calculoController.updateVeiculo);
router.post('/principal-condutor', calculoController.cadastroPrincipalCondutor);
router.put('/update-principal-condutor', calculoController.updatePrincipalCondutor);
router.post('/questionario-risco', calculoController.cadastroQuestionarioRisco);
router.put('/update-questionario-risco', calculoController.updateQuestionarioRisco);
router.post('/coberturas', calculoController.cadastroCoberturas);
router.put('/update-coberturas', calculoController.updateCoberturas);
router.get('/bot1/:idsegurado', botController.ativaBot1);
router.get('/bot2/:idsegurado', botController.ativaBot2);
router.get('/orcamentos/:idusuario', calculoController.getCalculo);
router.post('/busca-mapfre',botController.buscaMapfre);

module.exports = router;