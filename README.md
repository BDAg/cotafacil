# <div align="center">CotaFácil</div>

O objetivo desse projeto é construir um sistema multicálculo para corretoras de seguro no segmento de automóveis. Pretendemos disponibilizar o acesso ao cálculo de companhias de seguro existentes no mercado em um sistema unificado. Visamos automatizar parte do processo de fechamento de seguro, concedendo mais agilidade e facilidade do trabalho a ser executado pelo funcionário responsável, tendo em vista que este processo é repetitivo e demanda tempo, cujo qual poderia ser gasto em outras atividades de maior importância. O sistema será construído com base em web crawlers para automação do processo manual.

## [Wiki do Projeto](../../wikis/home)
