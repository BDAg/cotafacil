import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private api: string = "http://localhost:3000/"

  constructor(
    private http: HttpClient,
    ) { }

  createData() {

  }

  cadastroUsuario(body) {
    return this.http.post(`${this.api}usuarios/cadastro`,body);
  }

  login(body) {
    return this.http.post(`${this.api}usuarios/login`,body);
  }

  segurado(body) {
    return this.http.post(`${this.api}calculos/segurado`,body)
  }
  
  updateSegurado(body) {
    return this.http.put(`${this.api}calculos/update-segurado`, body)
  }

  veiculo(body) {
    return this.http.post(`${this.api}calculos/veiculo`,body)
  }

  updateVeiculo(body) {
    return this.http.put(`${this.api}calculos/update-veiculos`, body)
  }

  principalCondutor(body) {
    return this.http.post(`${this.api}calculos/principal-condutor`,body)
  }

  updatePrincipalCondutor(body) {
    return this.http.put(`${this.api}calculos/update-principal-condutor`,body)
  }

  questionarioRisco(body) {
    return this.http.post(`${this.api}calculos/questionario-risco`,body)
  }

  updateQuestionarioRisco(body) {
    return this.http.put(`${this.api}calculos/update-questionario-risco`,body)
  }

  cobertura(body) {
    return this.http.post(`${this.api}calculos/coberturas`,body)
  }

  updatecobertura(body) {
    return this.http.put(`${this.api}calculos/update-coberturas`,body)
  }

  bot1(id) {
    return this.http.get(`${this.api}calculos/bot1/${id}`).toPromise()
  }

  bot2(id) {
    return this.http.get(`${this.api}calculos/bot2/${id}`).toPromise()
  }

  orcamentos(id) {
    return this.http.get(`${this.api}calculos/orcamentos/${id}`).toPromise()
  }
}
