import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatRippleModule} from '@angular/material/core';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';

import {NgxMaskModule} from 'ngx-mask';
import {CurrencyMaskModule} from 'ng2-currency-mask';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatSnackBarModule,
        MatRippleModule,
        MatTabsModule,
        MatListModule,
        MatCheckboxModule,
        MatStepperModule,
        MatButtonModule,
        MatSelectModule,
        MatRadioModule,
        NgxMaskModule,
        CurrencyMaskModule,
        MatCardModule

    ],
    exports: [
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatSnackBarModule,
        MatRippleModule,
        MatTabsModule,
        MatListModule,
        MatCheckboxModule,
        MatStepperModule,
        MatButtonModule,
        MatSelectModule,
        MatRadioModule,
        NgxMaskModule,
        CurrencyMaskModule,
        MatCardModule
    ]
})
export class MaterialModule {
}
