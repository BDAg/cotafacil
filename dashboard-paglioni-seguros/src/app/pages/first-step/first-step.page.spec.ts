import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FirstStepPage } from './first-step.page';

describe('FirstStepPage', () => {
  let component: FirstStepPage;
  let fixture: ComponentFixture<FirstStepPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstStepPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FirstStepPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
