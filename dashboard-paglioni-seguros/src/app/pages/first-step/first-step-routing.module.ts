import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstStepPage } from './first-step.page';

const routes: Routes = [
  {
    path: '',
    component: FirstStepPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstStepPageRoutingModule {}
