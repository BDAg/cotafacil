import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.page.html',
  styleUrls: ['./first-step.page.scss'],
})
export class FirstStepPage implements OnInit {

  constructor(
      private router: Router
  ) { }

  ngOnInit() {
  }

  next() {
    return this.router.navigateByUrl('/sign-up').then();
  }

  skip() {
    this.visualized();
    this.router.navigateByUrl('/sign-in').then();
  }

  visualized() {
    localStorage.setItem('welcomePage', JSON.stringify({ isVisualized: true }));
  }

}
