import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FirstStepPageRoutingModule } from './first-step-routing.module';

import { FirstStepPage } from './first-step.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FirstStepPageRoutingModule
  ],
  declarations: [FirstStepPage]
})
export class FirstStepPageModule {}
