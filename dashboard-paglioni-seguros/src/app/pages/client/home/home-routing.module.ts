import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomePage } from "./home.page";
const routes: Routes = [
  {
    path: "",
    component: HomePage,
  },
  {
    path: "quotes/choose",
    loadChildren: () =>
      import("./components/quotes/choose/choose.module").then(
        (m) => m.ChoosePageModule
      ),
  },
  {
    path: "quotes/auto-insurance",
    loadChildren: () =>
      import("./components/quotes/auto-insurance/auto-insurance.module").then(
        (m) => m.AutoInsurancePageModule
      ),
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes),],
  exports: [RouterModule,],
})
export class HomePageRoutingModule {}
