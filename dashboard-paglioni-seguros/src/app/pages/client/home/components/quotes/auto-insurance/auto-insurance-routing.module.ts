import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutoInsurancePage } from './auto-insurance.page';

const routes: Routes = [
  {
    path: '',
    component: AutoInsurancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AutoInsurancePageRoutingModule {}
