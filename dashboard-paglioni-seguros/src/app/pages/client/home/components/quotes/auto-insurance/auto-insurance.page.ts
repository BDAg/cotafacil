import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { map, tap } from "rxjs/operators";
import { ApiService } from '../../../../../../services/api.service';

@Component({
  selector: 'app-auto-insurance',
  templateUrl: './auto-insurance.page.html',
  styleUrls: ['./auto-insurance.page.scss'],
})
export class AutoInsurancePage implements OnInit {
  step = 1;

  condutor = 0;
  firstStep: FormGroup;
  segurado: FormGroup;
  condutorPrincipal: FormGroup;
  questionarioRisco: FormGroup;
  coberturas: FormGroup;
  coberturas2: FormGroup;
  firstStepValid = false;
  contadorSegurado = 0;
  contadorVeiculos = 0;
  contadorCondutor = 0;
  contadorQuestionarioRisco = 0;
  contadorCoberturas = 0;
  mapfreValor;
  mapfreReduzida;
  hdiReduzida;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: ApiService
  ) {
  }

  ngOnInit() {

    /* First Step */
    this.firstStep = this.formBuilder.group({
      placa: ['', Validators.minLength(3)],
      chassi: ['', Validators.minLength(3)],
      cepPernoite: ['', Validators.minLength(3)],
      veiculo: ['', Validators.minLength(3)],
      anoFabricacao: [''],
      anoModelo: [''],
    });

    this.segurado = this.formBuilder.group({
      cpf: [''],
      nome: [''],
      data_nascimento: ['']
    })

    this.condutorPrincipal = this.formBuilder.group({
      segurado: ["1"],
      cpf: [""],
      nome: [""],
      sexo: [""],
      nascimento: [""],
      estadoCivil: [""],
      idveiculos: [""]
    })

    this.questionarioRisco = this.formBuilder.group({
      utilizacao: ["locomoção diaria"],
      menor_condutor: ['1'],
      garagem_casa: ['1'],
      garagem_servico: ['1'],
      garagem_escola: ['1'],
      idveiculos: [""],
    })

    this.coberturas = this.formBuilder.group({
      danos_morais_esteticos: [""],
      danos_materiais: [""],
      danos_corporais: [""],
      morte_invalidez_ocupante: [""]
    })

    this.coberturas2 = this.formBuilder.group({
      limite_guincho: [""],
      carro_reserva: [""],
      vidros: [""],
      farol_auxiliar: ["nada"],
      retrovisor_lanterna_farol: ["nada"],
    })



    this.firstStep.statusChanges.pipe(
      map(isValid => isValid === 'VALID' ? this.firstStepValid = true : this.firstStepValid = false),
    ).subscribe();
  }

  next(step) {
    if (step === 1) {
      console.log(this.segurado)
      let body = {
        idsegurados: localStorage.getItem('idSegurado'),
        cpf: this.segurado.controls.cpf.value,
        nome: this.segurado.controls.nome.value,
        data_nascimento: this.segurado.controls.data_nascimento.value,
      }
  
      if (this.contadorSegurado == 0) {
        this.apiService.segurado(body).subscribe(data => {
          localStorage.setItem('idSegurado', data['results']);
          console.log('salvou segurado')
          console.log(data)
          this.contadorSegurado = 1;
        })
      }
      else {
        this.apiService.updateSegurado(body).subscribe(data => {
          console.log(data)
          console.log('alterou segurado')
        })
      }
      this.step += 1;
    }
  
    if (step === 2) {
      let body = {
        chassi: this.firstStep.controls.chassi.value,
        placa: this.firstStep.controls.placa.value,
        ano_fabricacao: this.firstStep.controls.anoFabricacao.value,
        ano_modelo: this.firstStep.controls.anoModelo.value,
        veiculos: this.firstStep.controls.veiculo.value,
        cep_pernoite: this.firstStep.controls.cepPernoite.value,
        idveiculos: localStorage.getItem('idVeiculo'),
        idsegurados: localStorage.getItem('idSegurado'),
      }
      if (this.contadorVeiculos == 0) {
        this.apiService.veiculo(body).subscribe(data => {
          localStorage.setItem('idVeiculo', data['results']);
          this.contadorVeiculos = 1;
          console.log('salvou veiculo')
          console.log(data)
        })
      } else {
        console.log('alterou veiculo')
        console.log(body)
        this.apiService.updateVeiculo(body).subscribe(data => {
          console.log(data)
        })
      }
      console.log(body)
      this.step += 1;
    }
  
    if (step === 3) {
      console.log(this.condutorPrincipal)
      if (this.contadorCondutor == 0) {
  
        if (this.condutorPrincipal.controls.segurado.value == 0) {
          let body = {
            cpf: this.condutorPrincipal.controls.cpf.value,
            nome: this.condutorPrincipal.controls.nome.value,
            data_nascimento: this.condutorPrincipal.controls.nascimento.value,
            sexo: this.condutorPrincipal.controls.sexo.value,
            estado_civil: this.condutorPrincipal.controls.estadoCivil.value,
            segurado: this.condutorPrincipal.controls.segurado.value,
            idveiculos: localStorage.getItem('idVeiculo')
          }
  
          this.apiService.principalCondutor(body).subscribe(data => {
            console.log(data)
            console.log('salvou principal condutor')
          })
          console.log(body)
        } else {
          let body = {
            segurado: this.condutorPrincipal.controls.segurado.value,
            cpf: 'segurado',
            nome: 'segurado',
            data_nascimento: 'segurado',
            sexo: this.condutorPrincipal.controls.sexo.value,
            estado_civil: this.condutorPrincipal.controls.estadoCivil.value,
            idveiculos: localStorage.getItem('idVeiculo')
          }
          this.apiService.principalCondutor(body).subscribe(data => {
            console.log(data)
            console.log('salvou principal condutor')
          })
        }
  
        this.contadorCondutor = 1;
      } else {
        if (this.condutorPrincipal.controls.segurado.value == 0) {
          let body = {
            cpf: this.condutorPrincipal.controls.cpf.value,
            nome: this.condutorPrincipal.controls.nome.value,
            data_nascimento: this.condutorPrincipal.controls.nascimento.value,
            sexo: this.condutorPrincipal.controls.sexo.value,
            estado_civil: this.condutorPrincipal.controls.estadoCivil.value,
            segurado: this.condutorPrincipal.controls.segurado.value,
            idveiculos: localStorage.getItem('idVeiculo')
          }
  
          this.apiService.updatePrincipalCondutor(body).subscribe(data => {
            console.log(data)
            console.log('alterou principal condutor')
          })
        } else {
          let body = {
            segurado: this.condutorPrincipal.controls.segurado.value,
            cpf: 'segurado',
            nome: 'segurado',
            data_nascimento: 'segurado',
            sexo: this.condutorPrincipal.controls.sexo.value,
            estado_civil: this.condutorPrincipal.controls.estadoCivil.value,
            idveiculos: localStorage.getItem('idVeiculo')
          }
          this.apiService.updatePrincipalCondutor(body).subscribe(data => {
            console.log(data)
            console.log('alterou principal condutor')
          })
        }
  
      }
      this.step += 1;
    }
  
    if (step === 4) {
      let body = {
        utilizacao: this.questionarioRisco.controls.utilizacao.value,
        menor_condutor: this.questionarioRisco.controls.menor_condutor.value,
        garagem_casa: this.questionarioRisco.controls.garagem_casa.value,
        garagem_servico: this.questionarioRisco.controls.garagem_servico.value,
        garagem_escola: this.questionarioRisco.controls.garagem_escola.value,
        idveiculos: localStorage.getItem('idVeiculo')
      }
      if (this.contadorCoberturas == 0) {
        this.apiService.questionarioRisco(body).subscribe(data => {
          console.log(data)
          console.log('adicionou questionario de risco')
        })
      } else {
        this.apiService.updateQuestionarioRisco(body).subscribe(data => {
          console.log(data)
          console.log('alterou questionario de risco')
        })
      }
      this.step += 1;
    }
  
    if (step === 5) {
      console.log(this.coberturas)
  
      this.step += 1;
    }
  
    if (step === 6) {
      console.log(this.coberturas2)
      let body = {
        danos_morais_esteticos: this.coberturas.controls.danos_morais_esteticos.value,
        danos_materiais: this.coberturas.controls.danos_materiais.value,
        danos_corporais: this.coberturas.controls.danos_corporais.value,
        morte_invalidez_ocupante: this.coberturas.controls.morte_invalidez_ocupante.value,
        limite_guincho: this.coberturas2.controls.limite_guincho.value,
        carro_reserva: this.coberturas2.controls.carro_reserva.value,
        vidros: this.coberturas2.controls.vidros.value,
        farol_auxiliar: this.coberturas2.controls.farol_auxiliar.value,
        retrovisor_lanterna_farol: this.coberturas2.controls.retrovisor_lanterna_farol.value,
        idveiculos: localStorage.getItem('idVeiculo')
      }
      if (this.contadorCoberturas == 0) {
        this.apiService.cobertura(body).subscribe(data => {
          console.log(data)
          console.log('adicionar cobertura')
        })
        this.contadorCoberturas = 1;
      } else {
        
        this.apiService.updatecobertura(body).subscribe(data => {
          console.log(data)
          console.log('alterou a cobertura')
        })
      }
  
      this.step += 1;
    }
    if (step === 7) {
      this.router.navigateByUrl('/client-app/home').then(() => {
        this.contadorSegurado = 0;
        this.contadorVeiculos = 0;
        this.contadorCondutor = 0;
        this.contadorQuestionarioRisco = 0;
        this.contadorCoberturas = 0;
        this.step = 1;
        this.mapfreValor = 'calcular';
        this.mapfreReduzida = 'calcular';
        this.hdiReduzida = 'calcular';
        this.firstStep.reset()
        this.segurado.reset()
        this.condutorPrincipal.reset()
        this.questionarioRisco.reset()
        this.coberturas.reset()
        this.coberturas2.reset()
      });
      
    }
  
  }
  bot() {
    this.apiService.bot1(localStorage.getItem('idSegurado')).then(data => {
      setTimeout(()=> {
        this.mapfreValor = data[1]
        this.mapfreReduzida = data[0]
        console.log(data)
      },500)
    })
  }
  bot2() {
    this.apiService.bot2(localStorage.getItem('idSegurado')).then(data => {
      setTimeout(()=> {
        this.hdiReduzida = data[0]
      },500)
    })
  }
  
  back(step) {
  
    switch (step) {
      case 1:
        this.router.navigateByUrl('/client-app/home/quotes/choose').then();
        break;
      case 2:
        this.step -= 1;
        break;
      case 3:
        this.step -= 1;
        break;
      case 4:
        this.step -= 1;
        break;
      case 5:
        this.step -= 1;
        break;
      case 6:
        this.step -= 1;
        break;
      case 7:
        this.step -= 1;
        break;
    }
  
  }
}
