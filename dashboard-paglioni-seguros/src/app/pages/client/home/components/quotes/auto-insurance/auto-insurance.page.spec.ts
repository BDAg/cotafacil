import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AutoInsurancePage } from './auto-insurance.page';

describe('AutoInsurancePage', () => {
  let component: AutoInsurancePage;
  let fixture: ComponentFixture<AutoInsurancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoInsurancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AutoInsurancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
