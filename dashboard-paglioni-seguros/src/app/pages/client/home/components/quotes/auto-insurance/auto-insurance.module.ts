import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {AutoInsurancePageRoutingModule} from './auto-insurance-routing.module';

import {AutoInsurancePage} from './auto-insurance.page';
import {MaterialModule} from "../../../../../../shared/material/material.module";
import { MatSliderModule } from '@angular/material/slider';

import { BrMaskerModule } from 'br-mask';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        AutoInsurancePageRoutingModule,
        MaterialModule,
        MatSliderModule,
        BrMaskerModule
    ],
    declarations: [AutoInsurancePage]
})
export class AutoInsurancePageModule {
}
