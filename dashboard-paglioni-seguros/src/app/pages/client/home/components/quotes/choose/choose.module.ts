import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ChoosePageRoutingModule} from './choose-routing.module';

import {ChoosePage} from './choose.page';
import {MaterialModule} from "../../../../../../shared/material/material.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        ChoosePageRoutingModule,
        MaterialModule
    ],
    declarations: [ChoosePage]
})
export class ChoosePageModule {
}
