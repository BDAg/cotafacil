import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-choose',
  templateUrl: './choose.page.html',
  styleUrls: ['./choose.page.scss'],
})
export class ChoosePage implements OnInit {
  tipoSeguro : FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.tipoSeguro = this.formBuilder.group({
      tipo : ['']
    })
  }

  redirecionar() {
    this.router.navigateByUrl('/client-app/home/quotes/auto-insurance').then()
    console.log(this.tipoSeguro)
  }
  
  ngOnDestroy() {
    console.log('ngOnDestroy: cleaning up...');
    this.tipoSeguro.reset()
  }
}
