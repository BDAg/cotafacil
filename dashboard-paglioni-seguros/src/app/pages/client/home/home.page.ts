import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  teste = 1;
  orcamentos;

  constructor(
    private apiService: ApiService
  ) { }
  ngOnInit() {
    
  }
  ionViewWillEnter() {
    this.apiService.orcamentos(localStorage.getItem('idusuario')).then( data => {
      this.orcamentos = data['results']
      console.log(this.orcamentos)
    })
  }
  ngOnDestroy() {

  }

}
