import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(
      private router: Router,
      private apiService: ApiService
  ) { 
  }

  ngOnInit() {
    
  }

  readData() {
    
  }

  next() {
      this.visualized();
      return this.router.navigateByUrl('/first-step').then();
  }

  skip() {
    this.visualized();
    this.router.navigateByUrl('/sign-in').then();
  }

  visualized() {
    localStorage.setItem('welcomePage', JSON.stringify({ isVisualized: true }));
  }

}
