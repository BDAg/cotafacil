import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {
  form: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private apiService: ApiService
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['']
    });
  }

 
  onSubmit() {
    let body = {
      email: this.form.controls.email.value,
      senha: this.form.controls.password.value
    }
    this.apiService.login(body).subscribe(data => {
      if( data['mensagem']) {
        console.log(data['mensagem'])
        alert(data['mensagem'])
      } else if(data['result'][0]['nome']) {
        localStorage.setItem('idusuario', data['result'][0]['idusuarios']);
        localStorage.setItem('nome',data['result'][0]['nome']);
        localStorage.setItem('email',data['result'][0]['email']);
        localStorage.setItem('cel',data['result'][0]['cel']);
        this.router.navigateByUrl('/client-app/home').then();
      }

    })
  }

}