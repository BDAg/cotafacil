import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap, map } from 'rxjs/operators';
import { ApiService } from '../../services/api.service';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.page.html',
    styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

    step = 1;

    firstStep: FormGroup;
    firstStepValid = false;

    secondStep: FormGroup;
    secondStepValid = false;

    thirdStep: FormGroup;
    thirdStepValid = false;

    fourthStep: FormGroup;
    fourthStepValid = false;

    fifthStep: FormGroup;
    fifthStepValid = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private apiService: ApiService
    ) {
    }

    ngOnInit() {

        /* First Step */
        this.firstStep = this.formBuilder.group({
            nome: [''],
            email: [''],
            cel: [''],
            senha: ['']
        });
    }

    cadastro(body) {
        body = {

            nome: this.firstStep.controls.nome.value,
            email: this.firstStep.controls.email.value,
            cel: this.firstStep.controls.cel.value,
            senha: this.firstStep.controls.senha.value

        }
        console.log(body)
        this.apiService.cadastroUsuario(body).subscribe(data => {
            console.log(data)
        })
    }

    next(step) {
        if (step === 1) {
            this.step += 1;
        }

        if (step === 2) {
            this.step += 1;
        }

        if (step === 3) {
            this.step += 1;
        }

        if (step === 4) {
            console.log(this.firstStep)
            this.cadastro(this.firstStep)
            this.router.navigateByUrl('/sign-in').then();
        }

    }

    back(step) {
        switch (step) {
            case 1:
                this.router.navigateByUrl('/first-step').then();
                break;
            case 2:
                this.step -= 1;
                break;
            case 3:
                this.step -= 1;
                break;
            case 4:
                this.step -= 1;
                break;
            case 5:
                this.step -= 1;
                break;
        }

    }
}
