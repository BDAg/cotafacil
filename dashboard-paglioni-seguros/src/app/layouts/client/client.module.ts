import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ClientPageRoutingModule} from './client-routing.module';

import {ClientPage} from './client.page';
import {MaterialModule} from "../../shared/material/material.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ClientPageRoutingModule,
        MaterialModule
    ],
    declarations: [ClientPage]
})
export class ClientPageModule {
}
