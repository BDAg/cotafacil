import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientPage } from './client.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'client-app/home'
  },
  {
    path: '',
    component: ClientPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../../pages/client/home/home.module').then( m => m.HomePageModule)
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientPageRoutingModule {}
